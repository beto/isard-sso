#!/bin/bash
set -e

#git submodule init
#git submodule update --recursive --remote

cp main.conf .env

## BUILD_ROOT_PATH env
# This is a workarround for
# https://github.com/docker/compose/issues/7873
# See also BUILD_ROOT_PATH sed section at the end of file
echo "BUILD_ROOT_PATH=$(pwd)" >> .env

. ./.env

cp .env docker-compose-parts/
docker-compose -f docker-compose-parts/haproxy.yml -f docker-compose-parts/freeipa.yml -f docker-compose-parts/keycloak.yml -f docker-compose-parts/postgresql.yml -f docker-compose-parts/network.yml config > docker-compose.yml 
