sh /opt/jboss/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user admin --password k3ycl0ak
#./kcadm.sh get clients/5e7c2693-2a14-4166-8488-5258c8491688 -r master
sh /opt/jboss/keycloak/bin/kcadm.sh create clients -r poc -s clientId=moodle \
    -s enabled=true \
    -s clientAuthenticatorType=client-secret \
    -s secret=d0b8122f-8dfb-46b7-b68a-f5cc4e25d000 \
    -s rootUrl="https://moodle.santantoni.duckdns.org" \
    -s adminUrl="https://moodle.santantoni.duckdns.org" \
    -s 'redirectUris=["https://moodle.santantoni.duckdns.org/*"]' \
    -s 'webOrigins=["https://moodle.santantoni.duckdns.org"]'

#### https://github.com/adorsys/keycloak-config-cli
#### https://medium.com/@jkroepke/handle-keycloak-config-as-code-d9265cdc03ad
## SAML2 Integration
#curl https://moodle.isardvdi.site/auth/saml2/sp/metadata.php

# Import as client provider
